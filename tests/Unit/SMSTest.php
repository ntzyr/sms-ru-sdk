<?php


namespace BonchDev\SMSRUSDK\Tests\Unit;


use BonchDev\SmsRuSDK\SendException;
use BonchDev\SMSRUSDK\SMS;
use BonchDev\SMSRUSDK\Tests\TestCase;

class SMSTest extends TestCase
{
    /**
     * @var SMS
     */
    private $sms;

    protected function setUp(): void
    {
        parent::setUp();

        $this->sms = new SMS(
            'your key here'
        );
    }

    public function testAuthCheckSuccess()
    {
        $response = $this->sms->check();

        $this->assertTrue(
            is_array($response->jsonResponse)
        );
    }

    public function testSMSSendFailAPI()
    {
        try {
            $response = $this->sms
                ->to('79999999999')
                ->test(1)
                ->msg('message')
                ->send();
        } catch (SendException $exception) {
            $this->assertTrue(
                is_array($exception->getFailedMessages())
            );
        }
    }
}