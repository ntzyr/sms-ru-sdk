<?php


namespace BonchDev\SMSRUSDK;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

class SMS
{
    public $query = [];
    /**
     * @var \Psr\Http\Message\ResponseInterface
     */
    public $response;
    public $jsonResponse = [];
    /**
     * @var array
     */
    public $failedMessages = [];

    /**
     * SMS constructor.
     * @param string $apiId
     * @param bool $json
     */
    public function __construct(
        string $apiId,
        bool $json = true,
        bool $test = false
    )
    {
        $this->query += [
            'api_id' => $apiId,
            'json' => (int) $json,
            'test' => (int) $test,
        ];

        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function __set($name, $value)
    {
        $this->query[$name] = $value;

        return $this;
    }

    /**
     * @param $name
     * @param $arguments
     * @return SMS
     */
    public function __call($name, $arguments)
    {
        return $this->__set($name, implode(',', $arguments));
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->query[$name];
    }

    /**
     * @return Client
     */
    private function client()
    {
        $client = new Client([
            'base_uri' => 'https://sms.ru/'
        ]);

        return $client;
    }

    /**
     * Setter for Json Response
     */
    private function setJsonResponse(): void
    {
        $this->jsonResponse = json_decode(
            $this->response->getBody()->getContents(),
            true
        );
    }

    /**
     * @param array $query
     */
    public function setQuery(array $query): void
    {
        $this->query = $query;
    }

    /**
     * @return $this
     */
    public function check()
    {
        $this->response = ($this->client())
            ->post(
                'auth/check',
                [
                    'query' => $this->query
                ]
            );

        $this->setJsonResponse();

        return $this;
    }

    /**
     * @return $this
     * @throws RequestException
     * @throws SendException
     */
    public function send()
    {
        $this->response = ($this->client())
            ->post(
                'sms/send',
                [
                    'query' => $this->query
                ]
            );

        $this->setJsonResponse();
        $this->handleExceptions();

        return $this;
    }

    /**
     * @throws RequestException
     * @throws SendException
     */
    private function handleExceptions()
    {
        if(
            $this->jsonResponse['status'] === 'ERROR'
        ) {
            throw new RequestException(
                $this->response,
                $this->jsonResponse
            );
        }

        if(
            isset($this->jsonResponse['sms'])
        ) {
            foreach ($this->jsonResponse['sms'] as $phone => $sms)
            {
                if($sms['status'] === "ERROR") {
                    $this->failedMessages[$phone] = $sms;
                }
            }

            if(count($this->failedMessages) > 0) {
                throw new SendException(
                    $this->response,
                    $this->jsonResponse,
                    $this->failedMessages
                );
            }
        }
    }
}