<?php


namespace BonchDev\SMSRUSDK;


class RequestException extends \Exception
{
    private $response;
    /**
     * @var array
     */
    private $jsonResponse;

    public function __construct(
        $response,
        array $jsonResponse
    )
    {
        parent::__construct(
            $jsonResponse['status_text'],
            $jsonResponse['status_code']
        );

        $this->response = $response;
        $this->jsonResponse = $jsonResponse;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getJsonResponse()
    {
        return $this->jsonResponse;
    }
}